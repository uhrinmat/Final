Jersey 2.5 RestFul WebService JAX-RS + JPA 2.1 + EclipseLink with Derby in memory database.

How to:
There's no need to install any DB or run any sql script 

1. mvn clean install
2. asadmin deploy target/final war
3. install js dependencies
    * cd /src/main/webapp/
    * if you do not have npm, install it, see detail here: https://github.com/npm/npm
    * npm install
    * node_modules containing all necessary libraries should be created in the webapp directory

Some expample actions:

Create some destinations POST *
http://localhost:8080/final/rest/destination
[data]
{"id":"null", "name":"Kuala Lumpur", "lat":1.5, "lon":1.0}

-keep in mind that name has to be unique for each destination.
-using id-null will force DB to assign it's own ID(Autoincrement) 

This is also fine:
{"id":13, "name":"Kuala Lumpur", "lat":1.5, "lon":1.0}

Get all destinations GET
http://localhost:8080/final/rest/destination

Modify destinations PUT
http://localhost:8080/final/rest/destination/1
{"id":1, "name":"Robert", "lat":1.5, "lon":1.0}

DELETE
http://localhost:8080/final/rest/destination/1

Create some flights(you need at least 2 destinations)

http://localhost:8080/final/rest/flight POST
{"id":null, "name":"747", "dateOfDeparture":"2012-04-21T18:25:43-05:00", "distance":1, "fromId":1, "toId":2}

To make things easier most of the fields can be null for now, unless you see it in assignment.

Some others -- all are according to specs

http://localhost:8080/final/rest/reservation
{"id":"null", "seats":100, "password":"heslo", "created":"2012-04-21T18:25:43-05:00", "state":"NEW", "url":"www.google.com"}

http://localhost:8080/final/rest/reservation GET all reservations
