package oi.fel.cvut.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@ServerEndpoint("/active-sessions")
public class ActiveSessionsService {
    private static final Logger log = LoggerFactory.getLogger(ActiveSessionsService.class);
    private static final Set<Session> activeSessions = new HashSet<>();
    private static final Lock sessionLock = new ReentrantLock();

    private void notifyActiveSessions() {
        log.info("Notifying all active sessions ({}).", activeSessions.size());

        activeSessions.forEach(session -> {
            try {
                session.getBasicRemote().sendText(Integer.toString(activeSessions.size()));
            } catch (IOException e) {
                log.error("Notification failed!", e);
            }
        });
    }

    @OnOpen
    public void onOpen(Session session) throws IOException {
        sessionLock.lock();
        try {
            activeSessions.add(session);
            notifyActiveSessions();

            log.info("New session '{}' opened.", session.getId());
        } finally {
            sessionLock.unlock();
        }
    }

    @OnClose
    public void onClose(Session session) throws IOException {
        sessionLock.lock();
        try {
            activeSessions.remove(session);
            notifyActiveSessions();

            log.info("Session '{}' closed.", session.getId());
        } finally {
            sessionLock.unlock();
        }
    }

    @OnError
    public void onError(Throwable t) {
        log.error("Web socket error!", t);
    }
}
