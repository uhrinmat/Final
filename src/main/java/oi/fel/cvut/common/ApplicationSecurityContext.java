package oi.fel.cvut.common;

import oi.fel.cvut.model.Person;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

/**
 * Created by hack006 on 2.12.15.
 */
public class ApplicationSecurityContext implements SecurityContext {
    private Person person;
    private String scheme;

    public ApplicationSecurityContext(@NotNull Person person, @NotNull String scheme) {
        this.person = person;
        this.scheme = scheme;
    }

    @Override
    public Principal getUserPrincipal() {
        return person;
    }

    @Override
    public boolean isUserInRole(String role) {
        return this.person.getRole().name().equals(role);
    }

    @Override
    public boolean isSecure() {
        return "https".equals(this.scheme);
    }

    @Override
    public String getAuthenticationScheme() {
        return SecurityContext.BASIC_AUTH;
    }
}
