package oi.fel.cvut.common;

import oi.fel.cvut.config.ApplicationConfig;

/**
 * Created by hack006 on 2.12.15.
 */
public enum UserRole {
    CLIENT(ApplicationConfig.CLIENT_ROLE), MANAGER(ApplicationConfig.MANAGER_ROLE), ADMIN(ApplicationConfig.ADMIN_ROLE);

    private String stringRepresentation;

    UserRole(String stringRepresentation) {
        this.stringRepresentation = stringRepresentation;
    }

    public String getStringRepresentation() {
        return stringRepresentation;
    }
}