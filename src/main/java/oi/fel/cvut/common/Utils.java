package oi.fel.cvut.common;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.SecurityContext;
import java.util.Base64;

/**
 * Created by hack006 on 2.12.15.
 */
public class Utils {
    public static Credentials base64StringToCredentils(@NotNull String base64EncodedString) throws IllegalArgumentException {
        String[] credentialParts = new String(Base64.getDecoder().decode(base64EncodedString)).split(":");

        if (credentialParts.length != 2) {
            throw new IllegalArgumentException("Base64 encoded string has invalid format. Format <username>:<password> is required!");
        }

        return new Credentials(credentialParts[0], credentialParts[1]);
    }

    public static boolean userHasAnyRole(SecurityContext securityContext, UserRole ...userRoles) {
        for (UserRole userRole : userRoles) {
            if (securityContext.isUserInRole(userRole.getStringRepresentation())) {
                return true;
            }
        }
        return false;
    }
}
