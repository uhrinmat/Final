package oi.fel.cvut.config;

import oi.fel.cvut.common.UserRole;
import oi.fel.cvut.model.Person;
import oi.fel.cvut.persistence.PersonDao;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

import java.util.logging.Logger;

/**
 * Created by matej on 11/11/15.
 */
public class ApplicationConfig extends ResourceConfig {
    private static final Logger log = Logger.getLogger(ApplicationConfig.class.getName());

    public static final String ADMIN_ROLE = "ADMIN";
    public static final String MANAGER_ROLE = "MANAGER";
    public static final String CLIENT_ROLE = "CLIENT";

    public ApplicationConfig() {
        packages(true, "oi.fel.cvut");
        // Allows @RolesAllowed annotation
        register(RolesAllowedDynamicFeature.class);

        dbSampleDataInit();
    }

    private void dbSampleDataInit() {
        PersonDao personDao = new PersonDao();

        try {
            Person admin = new Person("admin", "admin", UserRole.ADMIN);
            personDao.create(admin);

            Person client = new Person("client", "client", UserRole.CLIENT);
            personDao.create(client);

            log.info("Admin (admin:admin) and client(client:client) created.");
        } catch (Exception e) {
            log.info("Admin and client user names already exists in DB. Skipping creation ...");
        }
    }
}