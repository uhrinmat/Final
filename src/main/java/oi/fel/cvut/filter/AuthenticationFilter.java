package oi.fel.cvut.filter;

import oi.fel.cvut.common.ApplicationSecurityContext;
import oi.fel.cvut.common.Credentials;
import oi.fel.cvut.common.Secured;
import oi.fel.cvut.common.Utils;
import oi.fel.cvut.model.Person;
import oi.fel.cvut.persistence.PersonDao;

import javax.annotation.Priority;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * Created by hack006 on 2.12.15.
 */

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {
    public static final String PLAIN_AUTHENTICATION_METHOD = "Basic";

    @Override
    public void filter(ContainerRequestContext requestContext) throws WebApplicationException, IOException {
        PersonDao personDao = new PersonDao();
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        if (authorizationHeader == null || !authorizationHeader.startsWith(PLAIN_AUTHENTICATION_METHOD + " ")) {
            unauthorizedRequest(requestContext);
        }

        try {
            String base64AuthString = authorizationHeader.substring(PLAIN_AUTHENTICATION_METHOD.length()).trim();
            Credentials credentials = Utils.base64StringToCredentils(base64AuthString);

            Person person = personDao.findByUsernameAndPassword(credentials.getUsername(), credentials.getPassword());
            String scheme = requestContext.getUriInfo().getRequestUri().getScheme();

            requestContext.setSecurityContext(new ApplicationSecurityContext(person, scheme));
        } catch (Exception e) {
            unauthorizedRequest(requestContext);
        }
    }

    private void unauthorizedRequest(@NotNull ContainerRequestContext context) {
        Response response = Response.status(Response.Status.UNAUTHORIZED)
                .header(HttpHeaders.WWW_AUTHENTICATE, "Basic realm=\"Login to enter ;)\"")
                .entity("Page requires login.")
                .build();

        context.abortWith(response);
    }
}