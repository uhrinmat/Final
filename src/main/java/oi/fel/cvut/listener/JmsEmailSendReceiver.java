package oi.fel.cvut.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@MessageDriven(mappedName = "java:app/jms/airlineDestination", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class JmsEmailSendReceiver implements MessageListener {
    Logger log = LoggerFactory.getLogger(JmsEmailSendReceiver.class);

    public JmsEmailSendReceiver() {
        log.info("JMS ticket email sent message receiver initialized!");
    }

    @Override
    public void onMessage(Message message) {
        log.info("Message received");
        try {
            String messageTextBody = ((TextMessage) message).getText();
            System.out.println(messageTextBody);
        } catch (JMSException e) {
            log.error("Problem reading message body.", e);
        }
    }
}