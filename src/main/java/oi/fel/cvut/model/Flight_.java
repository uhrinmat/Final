package oi.fel.cvut.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by janaton1 on 11.11.15.
 */
@StaticMetamodel(Flight.class)
public class Flight_ {
    public static volatile SingularAttribute<Flight, String> name;
    public static volatile SingularAttribute<Flight, Date> dateOfDeparture;
    public static volatile SingularAttribute<Flight, Float> distance;
    public static volatile SingularAttribute<Flight, Integer> fromDestinationId;
    public static volatile SingularAttribute<Flight, Integer> toDestinationId;
}
