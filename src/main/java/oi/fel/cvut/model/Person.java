package oi.fel.cvut.model;

import lombok.Data;
import oi.fel.cvut.common.UserRole;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.security.Principal;
import java.util.Date;

/**
 * Created by hack006 on 2.12.15.
 */
@Entity
@Data
@XmlRootElement
public class Person implements Principal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String username;
    private String password;
    private Date created;

    @Enumerated(EnumType.STRING)
    private UserRole role;

    public Person() {
    }

    public Person(String username, String password, UserRole role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public String getName() {
        return this.username;
    }
}
