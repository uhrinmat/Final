package oi.fel.cvut.model;

import lombok.Data;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by matej on 11/11/15.
 */
@Entity
@Data
@XmlRootElement
public class Reservation implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int seats;
    private String password;
    private Date created;
    private Long flightId;

    @Enumerated(EnumType.STRING)
    private StateChoice state;

    @Transient
    private String url;

    public Long getId() {
        this.url = "/final/rest/reservation/" + id;
        return id;
    }

    public void setId(Long id) {
        this.id = id;
        this.url = "/final/rest/reservation/" + id;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Long getFlightId() {
        return flightId;
    }

    public void setFlightId(Long flightId) {
        this.flightId = flightId;
    }

    public StateChoice getState() {
        return state;
    }

    public void setState(StateChoice state) {
        this.state = state;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + getId() +
                ", seats=" + seats +
                ", password='" + password + '\'' +
                ", created=" + created +
                ", flightId=" + flightId +
                ", state=" + state +
                ", url='" + url + '\'' +
                '}';
    }
}
