package oi.fel.cvut.model;

/**
 * Created by matej on 11/11/15.
 */
public enum StateChoice {
    NEW, CANCELED, PAID
}
