package oi.fel.cvut.persistence;

import oi.fel.cvut.model.Destination;

/**
 * Created by matej on 11/11/15.
 */
public class DestinationDao extends GenericDaoImpl<Destination> implements GenericDao<Destination> {
}
