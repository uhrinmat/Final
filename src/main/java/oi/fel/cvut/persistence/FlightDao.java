package oi.fel.cvut.persistence;

import oi.fel.cvut.model.Flight;
import oi.fel.cvut.model.Flight_;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by matej on 11/11/15.
 */
public class FlightDao extends GenericDaoImpl<Flight> implements GenericDao<Flight> {
    public List<Flight> findAll(String name, Date departureFrom, Date departureTo) {
        EntityManager em = getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        List<Flight> listT = null;
        try {

            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<Flight> criteriaQuery = criteriaBuilder.createQuery(Flight.class);
            Root<Flight> flightRoot = criteriaQuery.from(Flight.class);
            List<Predicate> wherePredicates = new ArrayList<>();

            if (name != null) {
                wherePredicates.add(criteriaBuilder.equal(flightRoot.get(Flight_.name), name));
            }
            if (departureFrom != null) {
                wherePredicates.add(criteriaBuilder.greaterThan(flightRoot.get(Flight_.dateOfDeparture), departureFrom));
            }
            if (departureTo != null) {
                wherePredicates.add(criteriaBuilder.lessThan(flightRoot.get(Flight_.dateOfDeparture), departureTo));
            }

            Predicate[] wherePredicateArr = new Predicate[wherePredicates.size()];
            for(int i = 0; i < wherePredicates.size(); i++) {
                wherePredicateArr[i] = wherePredicates.get(i);
            }
            criteriaQuery.where(wherePredicateArr);

            listT = em.createQuery(criteriaQuery).getResultList();
            em.getTransaction().commit();
        } finally {
            em.close();
        }

        return listT;
    }
}
