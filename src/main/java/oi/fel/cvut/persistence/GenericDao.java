package oi.fel.cvut.persistence;

import java.util.List;

/**
 * Created by matej on 11/11/15.
 */
public interface GenericDao<T> {

    T create(T t);
    T delete(Object id);
    T find(Object id);
    List findAll();
    T update(T t);
}
