package oi.fel.cvut.persistence;

import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;


/**
 * Created by matej on 11/11/15.
 * The transactional approach and try catching is here only because @PersistenceContext annotation did not work.
 */
public abstract class GenericDaoImpl<T> implements GenericDao<T> {


    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("persist-unit");

    private Class<T> type;

    protected EntityManagerFactory getEntityManagerFactory() {
        return emf;
    }

    protected Class<T> getType() {
        return type;
    }

    public GenericDaoImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }

    public T create(final T t) {

        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();
            em.persist(t);
            transaction.commit();
        } catch (Exception e) {
            System.out.println("Error saving " + type.getName() + " : " + e.getMessage());
            transaction.rollback();
            return null;

        } finally {
            em.close();
        }

        return t;
    }

    public T find(Object id) {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        T t = null;
        try {
            t = em.find(type, id);
        } finally {
            em.close();
        }

        return t;
    }

    public List findAll() {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        List<T> listT = null;
        try {
            listT = em.createQuery("SELECT t FROM "+ type.getSimpleName() +" t").getResultList();
            em.getTransaction().commit();
        } finally {
            em.close();
        }

        return listT;
    }

    @Nullable
    public T update(T t) {

        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();
            em.merge(t);
            transaction.commit();
        } catch (Exception e) {
            System.out.println("Error updating " + type.getName() + " " + e.getMessage());
            transaction.rollback();
            return null;

        } finally {
            em.close();
        }

        return t;
    }

    public T delete(final Object id) {

        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        T t = null;

        try {
            transaction.begin();

            t = em.find(type, id);

            if(t == null) {
                System.out.println("Error Deleting " + type.getName() + ": not found");
            }
            else {
                em.remove(t);
            }

            transaction.commit();
        } catch (Exception e) {
            System.out.println("Error Deleting " + type.getName() + ": " + e.getMessage());
            transaction.rollback();
            return null;
        } finally {
            em.close();
        }
        return t;
    }

}
