package oi.fel.cvut.persistence;

import oi.fel.cvut.model.Person;

import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;

/**
 * Created by hack006 on 2.12.15.
 */
public class PersonDao extends GenericDaoImpl<Person> implements GenericDao<Person> {
    public Person findByUsernameAndPassword(@NotNull String username, @NotNull String password) {
        EntityManager em = getEntityManagerFactory().createEntityManager();
        Person person = null;

        try {
            person = em.createQuery("SELECT p " +
                    "FROM Person as p " +
                    "WHERE p.username = :username AND p.password = :password", Person.class)
                    .setParameter("username", username)
                    .setParameter("password", password)
                    .getSingleResult();
        } finally {
            em.close();
        }

        return person;
    }
}
