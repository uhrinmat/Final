package oi.fel.cvut.persistence;

import oi.fel.cvut.model.Reservation;

import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;

/**
 * Created by matej on 11/11/15.
 */
public class ReservationDao extends GenericDaoImpl<Reservation> implements GenericDao<Reservation> {
    public Reservation findByPasswordAndId(@NotNull String password, long id) {
        EntityManager em = getEntityManagerFactory().createEntityManager();
        Reservation reservation = null;

        try {
            reservation = em.createQuery("SELECT r " +
                    "FROM Reservation as r " +
                    "WHERE r.id = :id AND r.password = :password", Reservation.class)
                    .setParameter("id", id)
                    .setParameter("password", password)
                    .getSingleResult();
        } finally {
            em.close();
        }

        return reservation;
    }
}
