package oi.fel.cvut.resource;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderStatus;
import com.google.code.geocoder.model.LatLng;
import oi.fel.cvut.common.Secured;
import oi.fel.cvut.model.Destination;
import oi.fel.cvut.persistence.DestinationDao;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.List;

/**
 * Created by matej on 11/11/15.
 */
@Secured
@Path(value = "destination")
@XmlRootElement
public class DestinationResource {

    private DestinationDao dao;

    public DestinationResource() {
        dao = new DestinationDao();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Destination> findAll() {
        return dao.findAll();
    }

    @GET
    @Path(value = "{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Destination find(@PathParam("id") Long id) {
        return dao.find(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Destination create(Destination destination) {

        if (destination.getLat() == 0 && destination.getLon() == 0) { //If latitude and longtitude is at default value

            Geocoder geocoder = new Geocoder();
            GeocodeResponse geocoderResponse = null;
            GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(destination.getName()).setLanguage("en").getGeocoderRequest();
            try {
                geocoderResponse = geocoder.geocode(geocoderRequest);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (geocoderResponse.getStatus() == GeocoderStatus.OK) {
                LatLng location = geocoderResponse.getResults().get(0).getGeometry().getLocation();

                destination.setLat(location.getLat().floatValue());
                destination.setLon(location.getLng().floatValue());
            }
        }

        return dao.create(destination);
    }

    @PUT
    @Path(value = "{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Destination update(@PathParam("id") Long id, Destination destination) {
        destination.setId(id);
        return dao.update(destination);
    }

    @DELETE
    @Path(value = "{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Destination delete(@PathParam("id") Long id) {
        return dao.delete(id);
    }


}
