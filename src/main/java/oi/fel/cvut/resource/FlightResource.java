package oi.fel.cvut.resource;

import com.google.common.collect.Lists;
import oi.fel.cvut.common.Secured;
import oi.fel.cvut.model.Flight;
import oi.fel.cvut.persistence.DestinationDao;
import oi.fel.cvut.persistence.FlightDao;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by matej on 11/11/15.
 */
@Secured
@Path(value = "flight")
@XmlRootElement
public class FlightResource {
    private static final SimpleDateFormat FULL_ISO = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

    private FlightDao dao;
    private DestinationDao destinationDao;

    public FlightResource() {
        dao = new FlightDao();
        destinationDao = new DestinationDao();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Flight> findAll(@HeaderParam("X-Filter") String filterString) {
        Map<String,String> xFilters = Lists
                .newArrayList(filterString != null ? filterString.split(",") : new String[]{}).stream()
                .collect(Collectors.toMap(e -> e.split(":")[0], e -> e.split(":",2)[1]));


        Date dateOfDepartureFrom = null;
        try {
            dateOfDepartureFrom = FULL_ISO.parse(xFilters.get("dateOfDepartureFrom"));
        } catch (Exception e) {
            // ignore
        }
        Date dateOfDepartureTo = null;
        try {
            dateOfDepartureTo = FULL_ISO.parse(xFilters.get("dateOfDepartureTo"));
        } catch (Exception e) {
            // ignore
        }
        return dao.findAll(xFilters.get("name"),dateOfDepartureFrom , dateOfDepartureTo);
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON})
    public Flight create(Flight flight) {
        if (destinationDao.find(flight.getFromDestinationId()) == null ||
                destinationDao.find(flight.getToDestinationId()) == null ||
                flight.getFromDestinationId() == flight.getToDestinationId()) {
            throw new BadRequestException();
        }
        return dao.create(flight);
    }

    @GET
    @Path(value = "{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Flight find(@PathParam("id") Long id) {
        return dao.find(id);
    }

    @PUT
    @Path(value = "{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Flight update(@PathParam("id") Long id, Flight flight) {
        if (destinationDao.find(flight.getFromDestinationId()) == null ||
                destinationDao.find(flight.getToDestinationId()) == null ||
                        flight.getFromDestinationId() == flight.getToDestinationId()
                ) {
            throw new BadRequestException();
        }
        flight.setId(id); //Just to make sure there is no mismatch
        return dao.update(flight);
    }

    @DELETE
    @Path(value = "{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Flight delete(@PathParam("id") Long id) {
        return dao.delete(id);
    }

}
