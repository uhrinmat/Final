package oi.fel.cvut.resource;

import oi.fel.cvut.common.Secured;
import oi.fel.cvut.common.UserRole;
import oi.fel.cvut.common.Utils;
import oi.fel.cvut.config.ApplicationConfig;
import oi.fel.cvut.model.Reservation;
import oi.fel.cvut.model.StateChoice;
import oi.fel.cvut.persistence.ReservationDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ps.oi.fel.cvut.PrintService;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;

/**
 * Created by matej on 11/11/15.
 */
@Secured
@XmlRootElement
@Stateless
@Path(value = "reservation")
public class ReservationResource {
    private static final Logger log = LoggerFactory.getLogger(ReservationResource.class);

    private static final String X_PASSWORD_HEADER = "X-Password";
    private ReservationDao dao;

    @Resource(name = "java:app/jms/aosFactory")
    private ConnectionFactory connectionFactory;

    @Resource(name = "java:app/jms/ticketDestination")
    private Destination ticketDestination;

    public ReservationResource() {
        dao = new ReservationDao();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Reservation> findAll() {
        return dao.findAll();
    }

    @GET
    @Path(value = "{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Reservation find(@PathParam("id") Long id, @HeaderParam(X_PASSWORD_HEADER) String xPassword, @Context SecurityContext securityContext) {
        if (!Utils.userHasAnyRole(securityContext, UserRole.ADMIN, UserRole.MANAGER) && !isCorrectReservationPassword(xPassword, id)) {
            throw new NotAuthorizedException(Response.status(Response.Status.UNAUTHORIZED).build());
        }
        return dao.find(id);
    }

    @POST
    @RolesAllowed({ApplicationConfig.ADMIN_ROLE, ApplicationConfig.MANAGER_ROLE})
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Reservation create(Reservation r) {
        SecureRandom random = new SecureRandom();
        r.setState(StateChoice.NEW);
        r.setPassword(new BigInteger(130, random).toString(32));
        r.setCreated(new Date());
        return dao.create(r);
    }

    @PUT
    @Path(value = "{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Reservation update(@PathParam("id") Long id, Reservation r, @HeaderParam(X_PASSWORD_HEADER) String xPassword, @Context SecurityContext securityContext) {
        if (!Utils.userHasAnyRole(securityContext, UserRole.ADMIN, UserRole.MANAGER) && !isCorrectReservationPassword(xPassword, id)) {
            throw new NotAuthorizedException(Response.status(Response.Status.UNAUTHORIZED).build());
        }
        r.setId(id);

        Reservation savedReservation = dao.update(r);
        processConfirmationEmail(savedReservation);

        return savedReservation;
    }

    @DELETE
    @Path(value = "{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Reservation delete(@PathParam("id") Long id) {
        return dao.delete(id);
    }

    private boolean isCorrectReservationPassword(String reservationPassword, long reservationId) {
        try {
            dao.findByPasswordAndId(reservationPassword, reservationId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    // !!! Top down print service approach is not done - this is the old version
    @GET
    @Path("print/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response get(@PathParam("id") Long id) throws IOException {
        Reservation reservation = dao.find(id);

        String response = PrintService.createReservationConfirmationMessage(reservation);
        String filename = String.format("reservation_" + id);
        return Response.status(Response.Status.OK)
                .entity(response)
                .header("content-disposition", "attachment; filename = " + filename)
                .build();
    }

    private void processConfirmationEmail(@Nullable Reservation reservation) {
        if (reservation != null && reservation.getState().equals(StateChoice.PAID)) {
            try (JMSContext jmsContext = connectionFactory.createContext()) {
                jmsContext.createProducer().send(ticketDestination, reservation);
                log.info("Sending reservation {} to ticket service queue.", reservation.getId());
            }
        }
    }
}
