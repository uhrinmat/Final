package ps.oi.fel.cvut;

import oi.fel.cvut.model.Reservation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.*;

@MessageDriven(mappedName = "java:app/jms/ticketDestination", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class JmsTicketReceiver implements MessageListener {
    Logger log = LoggerFactory.getLogger(JmsTicketReceiver.class);

    @Resource(name = "java:app/jms/aosFactory")
    private ConnectionFactory connectionFactory;

    @Resource(name = "java:app/jms/airlineDestination")
    private Destination airlineDestination;

    public JmsTicketReceiver() {
        log.info("JMS ticket message receiver inicialized!");
    }

    @Override
    public void onMessage(Message message) {
        log.info("Message received");
        try (JMSContext jmsContext = connectionFactory.createContext()) {
            Reservation reservation = message.getBody(Reservation.class);

            // In real scenario PrintService.createReservationConfirmationMessage would also send the email message
            jmsContext.createProducer().send(airlineDestination, PrintService.createReservationConfirmationMessage(reservation));
            log.info("Reservation {} confirmation generated.", reservation.getId());
        } catch (JMSException e) {
            log.error("Problem reading message body.", e);
        }
    }
}