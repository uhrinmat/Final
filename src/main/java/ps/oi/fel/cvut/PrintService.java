package ps.oi.fel.cvut;

import oi.fel.cvut.model.Reservation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.mail.util.ByteArrayDataSource;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@WebService(serviceName = "PrintService", endpointInterface = "ps.oi.fel.cvut.PrintServiceWs")
public class PrintService implements PrintServiceWs{
    Logger log = LoggerFactory.getLogger(PrintService.class);

    @WebMethod
    public DataHandler print(@NotNull Reservation reservation) {
        String reservationConformationMessage = createReservationConfirmationMessage(reservation);

        ByteArrayDataSource dataSource = new ByteArrayDataSource(reservationConformationMessage.getBytes(), "text/plain");
        return new DataHandler(dataSource);
    }

    @NotNull
    public static String createReservationConfirmationMessage(@NotNull Reservation reservation) {
        StringBuilder sb = new StringBuilder();
        sb
                .append("--- TICK RESERVATION ---\n")
                .append("Reservation ID: ").append(reservation.getId()).append("\n")
                .append("Flight ID: ").append(reservation.getFlightId()).append("\n")
                .append("Your password: ").append(reservation.getPassword()).append("\n")
                .append("Seats: ").append(reservation.getSeats()).append("\n")
                .append("State: ").append(reservation.getState()).append("\n")
                .append("See more: ").append(reservation.getUrl()).append("\n")
                .append("Reservation created at: ").append(reservation.getCreated()).append("\n")
                .append("---------------------------").append("\n")
                .append("Generated at:").append(LocalDateTime.now());

        return sb.toString();
    }
}
