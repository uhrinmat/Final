package ps.oi.fel.cvut;

import oi.fel.cvut.model.Reservation;

import javax.activation.DataHandler;
import javax.jws.WebService;
import javax.validation.constraints.NotNull;

@WebService
public interface PrintServiceWs {
    DataHandler print(@NotNull Reservation reservation);
}