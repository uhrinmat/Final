var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require('angular2/angular2');
var router_1 = require('angular2/router');
var flights_1 = require("../flights/flights");
var destinations_1 = require("../destinations/destinations");
var reservations_1 = require("../reservations/reservations");
var common_1 = require("../common/common");
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        angular2_1.Component({
            selector: 'app',
            templateUrl: "app/components/app/main.html",
            directives: [router_1.ROUTER_DIRECTIVES, common_1.ActiveSessionsComponent],
        }),
        router_1.RouteConfig([
            { path: '/', as: 'Home', component: flights_1.FlightsComponent },
            // flights
            { path: '/flights', as: 'Flights', component: flights_1.FlightsComponent },
            { path: '/flight', as: 'NewFlight', component: flights_1.FlightComponent },
            { path: '/flight/:id', as: 'Flight', component: flights_1.FlightComponent },
            // destinations
            { path: '/destinations', as: 'Destinations', component: destinations_1.DestinationsComponent },
            { path: '/destination', as: 'NewDestination', component: destinations_1.DestinationComponent },
            { path: '/destination/:id', as: 'Destination', component: destinations_1.DestinationComponent },
            // reservations
            { path: '/reservations', as: 'Reservations', component: reservations_1.ReservationsComponent },
            { path: '/reservation', as: 'NewReservation', component: reservations_1.ReservationComponent },
            { path: '/reservation/:id', as: 'Reservation', component: reservations_1.ReservationComponent }
        ]), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
})();
exports.AppComponent = AppComponent;
//# sourceMappingURL=app-component.js.map