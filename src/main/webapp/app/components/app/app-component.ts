import { Component, View } from 'angular2/angular2';
import { ROUTER_DIRECTIVES, RouteConfig } from 'angular2/router';
import {FlightComponent, FlightsComponent} from "../flights/flights";
import {DestinationComponent, DestinationsComponent} from "../destinations/destinations";
import {ReservationComponent, ReservationsComponent} from "../reservations/reservations";
import {ActiveSessionsComponent} from "../common/common";

@Component({
    selector: 'app',
    templateUrl: "app/components/app/main.html",
    directives: [ROUTER_DIRECTIVES, ActiveSessionsComponent],
})
@RouteConfig([
    {path: '/', as: 'Home', component: FlightsComponent},
    // flights
    {path: '/flights', as: 'Flights', component: FlightsComponent},
    {path: '/flight', as: 'NewFlight', component: FlightComponent},
    {path: '/flight/:id', as: 'Flight', component: FlightComponent},
    // destinations
    {path: '/destinations', as: 'Destinations', component: DestinationsComponent},
    {path: '/destination', as: 'NewDestination', component: DestinationComponent},
    {path: '/destination/:id', as: 'Destination', component: DestinationComponent},
    // reservations
    {path: '/reservations', as: 'Reservations', component: ReservationsComponent},
    {path: '/reservation', as: 'NewReservation', component: ReservationComponent},
    {path: '/reservation/:id', as: 'Reservation', component: ReservationComponent}

])
export class AppComponent {

}