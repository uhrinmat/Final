import {Injectable, Injector, Observable, EventEmitter, Component, FORM_DIRECTIVES, NgFor, NgIf} from 'angular2/angular2';
import {MessageBus} from "../../../node_modules/angular2/ts/src/web_workers/shared/message_bus";
import ColumnFilter = ColumnFilters.ColumnFilter;
import {isNumber} from "angular2/src/core/facade/lang";
export const API_URL = "rest"; //'http://private-2141b-aossemestralwork.apiary-mock.com';

export class Pair<U,V> {
    public firstValue:U;
    public secondValue:V;

    constructor(first:U, second:V) {
        this.firstValue = first;
        this.secondValue = second;
    }
}

export class IdName<K> {
    public id:K;
    public name:string;

    constructor(id:K, name:string) {
        this.id = id;
        this.name = name;
    }
}

@Injectable()
export class CommonService {
    copyOwnObjectProperties(from:Object, to:Object):void {
        for (var field in from) {
            if (from.hasOwnProperty(field)) {
                to[field] = from[field];
            }
        }
    }
}

@Injectable()
export class messageService {
    messages:Message[];
    messageChanged:EventEmitter;

    constructor() {
        this.messages = [];
        this.messageChanged = new EventEmitter();
    }

    add(text:string, cssClass?:string) {
        var message:Message = new Message(text, cssClass);
        this.messages.push();
        this.messageChanged.next(message);
    }

    removeAll() {
        this.messages = [];
    }

    pickAll():Promise<Message[]> {
        // TODO revise this
        return new Promise<Message[]>((resolve) => {
            this.messageChanged.observer((value) => {
                resolve(this.messages);
                this.removeAll();
            });
        });
    }
}

@Component({
    selector: 'x-headers-filter',
    templateUrl: 'app/components/common/x-header-filter.html',
    directives: [FORM_DIRECTIVES, NgFor, NgIf],
    inputs: ["columns"],
    outputs: ['filtered']
})
export class XHeadersFilter {
    private static FILTER_DIRECTION_NONE = null;
    private static FILTER_DIRECTION_ASC = "asc";
    private static FILTER_DIRECTION_DESC = "desc";
    private static X_FILTER_HEADER = 'X-Filter';
    private static X_ORDER_HEADER = 'X-Order';
    private static X_BASE_HEADER = 'X-Base';
    private static X_OFFSET_HEADER = 'X-Offset';

    filtered:EventEmitter;
    columns:ColumnFilters.ColumnFilter[];

    constructor() {
        this.filtered = new EventEmitter();
    }

    filter():void {
        var headers = {};
        this.columns.forEach((column) => {
            if (column.value) {
                this.addHeaders(headers, XHeadersFilter.X_FILTER_HEADER, column.createFilterString());
            }
            if (column.orderDirection) {
                this.addHeaders(headers, XHeadersFilter.X_ORDER_HEADER, column.createOrderByString());
            }
        });
        console.log("filter value changed");
        this.filtered.next(headers);
    }

    changeOrderDirection(columnFilter:ColumnFilter) {
        // define order transitions
        switch (columnFilter.orderDirection) {
            case XHeadersFilter.FILTER_DIRECTION_ASC:
                columnFilter.orderDirection = XHeadersFilter.FILTER_DIRECTION_DESC;
                break;
            case XHeadersFilter.FILTER_DIRECTION_DESC:
                columnFilter.orderDirection = XHeadersFilter.FILTER_DIRECTION_NONE;
                break;
            default:
                columnFilter.orderDirection = XHeadersFilter.FILTER_DIRECTION_ASC;
        }
    }

    private addHeaders(headers:Object, header:string, value:string) {
        headers[header] = !headers[header] ? value : headers[header] + ',' + value;
    }
}

export module ColumnFilters {
    export abstract class ColumnFilter {
        name:string;
        orderDirection:string;
        type:string;
        value:any;

        constructor(name:string) {
            this.name = name;
            this.orderDirection = null;
            this.type = null;
            this.value = null;
        }

        createOrderByString():string {
            return this.orderDirection ? this.name + ":" + this.orderDirection : null;
        }

        abstract createFilterString():string;
    }

    export class StringColumnFilter extends ColumnFilter {
        value:string;

        constructor(name:string) {
            super(name);
            this.type = "text";
        }

        createFilterString():string {
            return this.name + ':' + this.value;
        }
    }

    export class DateTimeColumnFilter extends ColumnFilter {
        value:string;

        constructor(name:string) {
            super(name);
            this.type = "datetime";
        }

        createFilterString():string {
            return this.name + ':' + this.value;
        }
    }

    export class NumberColumnFilter extends ColumnFilter {
        value:number;
        equalityOperator:string;

        constructor(name:string) {
            super(name);
            this.type = "number";
            this.equalityOperator = "=";
        }

        createFilterString():string {
            return this.name + ":" + this.equalityOperator + this.value;
        }
    }
}

class Message {
    text:string;
    cssClass:string;

    constructor(text:string, cssClass?:string) {
        this.text = text;
        this.cssClass = cssClass || "info";
    }
}

@Component({
    selector: 'active-sessions',
    directives: [FORM_DIRECTIVES, NgFor, NgIf],
    providers: [],
    templateUrl: 'app/components/common/active-sessions.html'
})
export class ActiveSessionsComponent {
    // TODO hard encoded url
    static SESSIONS_ENDPOINT:string = 'ws:localhost:8080/final/active-sessions';
    webSocket:WebSocket;
    activeSessions:number;

    constructor() {
        var instance = this;
        this.activeSessions = 0;

        this.webSocket = new WebSocket(ActiveSessionsComponent.SESSIONS_ENDPOINT);
        this.webSocket.onmessage = function (message) {
            instance.activeSessions = message.data;
        };
    }
}