var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require('angular2/angular2');
var router_1 = require('angular2/router');
var detail_service_1 = require("../../services/detail-service");
var common_1 = require('../common/common');
var destination_service_1 = require("../destinations/destination-service");
var destination_service_2 = require("./destination-service");
var DestinationsComponent = (function () {
    function DestinationsComponent(destinationService) {
        var _this = this;
        this.title = 'Destinations';
        this.destinations = [];
        destinationService.loadDestinations().then(function (destinations) { return _this.destinations = destinations; });
    }
    DestinationsComponent = __decorate([
        angular2_1.Component({
            selector: 'destinations',
            directives: [router_1.RouterLink, angular2_1.NgFor],
            providers: [destination_service_1.DestinationService],
            templateUrl: 'app/components/destinations/destinations.html'
        }), 
        __metadata('design:paramtypes', [destination_service_1.DestinationService])
    ], DestinationsComponent);
    return DestinationsComponent;
})();
exports.DestinationsComponent = DestinationsComponent;
var DestinationComponent = (function () {
    function DestinationComponent(routeParams, detailService) {
        var _this = this;
        this.detailService = detailService;
        this.title = 'Destination detail';
        this.destination = new destination_service_2.Destination();
        var entityId = routeParams.get("id");
        if (entityId) {
            detailService.fetchDetail(entityId, DestinationComponent.ENTITY_ENDPOINT, destination_service_2.Destination).then(function (result) {
                _this.destination = result;
            });
        }
    }
    DestinationComponent.prototype.saveDestination = function (destination) {
        console.log("saving destination ...");
        this.detailService.saveDetail(destination, DestinationComponent.ENTITY_ENDPOINT);
    };
    DestinationComponent.prototype.deleteDestination = function (destination) {
        this.detailService.deleteEntity(destination, DestinationComponent.ENTITY_ENDPOINT);
    };
    DestinationComponent.ENTITY_ENDPOINT = common_1.API_URL + "/destination";
    DestinationComponent = __decorate([
        angular2_1.Component({
            selector: 'destination',
            directives: [angular2_1.FORM_DIRECTIVES, router_1.RouterLink, angular2_1.NgFor, angular2_1.NgIf],
            providers: [detail_service_1.DetailService],
            templateUrl: 'app/components/destinations/destination.html'
        }), 
        __metadata('design:paramtypes', [router_1.RouteParams, detail_service_1.DetailService])
    ], DestinationComponent);
    return DestinationComponent;
})();
exports.DestinationComponent = DestinationComponent;
//# sourceMappingURL=destinations.js.map