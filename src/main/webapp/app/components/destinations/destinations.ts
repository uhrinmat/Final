import { Component, View, NgFor, NgIf, NgClass, NgModel, FORM_DIRECTIVES, FormBuilder, ControlGroup, Validators } from 'angular2/angular2';
import { RouterLink, RouteParams, RouteConfig } from 'angular2/router';
import {Response, Http} from 'angular2/http';
import {EntityDetail, BaseEntityDetail, DetailService} from "../../services/detail-service";
import {API_URL} from '../common/common';
import {DestinationService} from "../destinations/destination-service";
import {FlightsComponent} from "../flights/flights";
import {Destination} from "./destination-service";

@Component({
    selector: 'destinations',
    directives: [RouterLink, NgFor],
    providers: [DestinationService],
    templateUrl: 'app/components/destinations/destinations.html'
})
export class DestinationsComponent {
    title:string;
    destinations:Destination[];

    constructor(destinationService:DestinationService) {
        this.title = 'Destinations';
        this.destinations = [];

        destinationService.loadDestinations().then((destinations) =>  this.destinations = <Destination[]>destinations);
    }
}

@Component({
    selector: 'destination',
    directives: [FORM_DIRECTIVES, RouterLink, NgFor, NgIf],
    providers: [DetailService],
    templateUrl: 'app/components/destinations/destination.html'
})
export class DestinationComponent {
    static ENTITY_ENDPOINT:string = API_URL + "/destination";

    private detailService:DetailService;

    title:string;
    destinationOptions:Object[];
    destination:Destination;

    constructor(routeParams:RouteParams, detailService:DetailService) {
        this.detailService = detailService;

        this.title = 'Destination detail';
        this.destination = new Destination();

        var entityId = routeParams.get("id");
        if (entityId) {
            detailService.fetchDetail(entityId, DestinationComponent.ENTITY_ENDPOINT, Destination).then((result) => {
                this.destination = <Destination>result
            });
        }
    }

    saveDestination(destination:Destination):void {
        console.log("saving destination ...");
        this.detailService.saveDetail(destination, DestinationComponent.ENTITY_ENDPOINT);
    }

    deleteDestination(destination:Destination):void {
        this.detailService.deleteEntity(destination, DestinationComponent.ENTITY_ENDPOINT);
    }
}