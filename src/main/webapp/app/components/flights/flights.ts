import { Component, View, NgFor, NgIf, NgClass, NgModel, FORM_DIRECTIVES, FormBuilder, ControlGroup, Validators } from 'angular2/angular2';
import { RouterLink, RouteParams, RouteConfig } from 'angular2/router';
import {Response, Http} from 'angular2/http';
import {EntityDetail, BaseEntityDetail, DetailService} from "../../services/detail-service";
import {API_URL,XHeadersFilter, ColumnFilters} from '../common/common';
import {DestinationService} from "../destinations/destination-service";
import {FlightService, Flight} from "./flight-service";

@Component({
    selector: 'flights',
    directives: [FORM_DIRECTIVES, RouterLink, NgFor, XHeadersFilter],
    providers: [FlightService,DestinationService],
    templateUrl: 'app/components/flights/flights.html'
})
export class FlightsComponent {
    private flightService:FlightService;

    title:string;
    flights:Flight[];
    destinationMap:Object;
    filterDefinition:ColumnFilters.ColumnFilter[];

    constructor(flightService:FlightService,destinationService:DestinationService) {
        this.flightService = flightService;

        this.title = 'Flights';
        this.flights = [];
        this.destinationMap = {};
        destinationService.loadDestinationOptions().then((destinationOptions) => {
            destinationOptions.forEach((destinationOption) => this.destinationMap[destinationOption.id] = destinationOption.name);
        });

        flightService.loadFlights().then((flights) => this.flights = flights);
        this.buildColumnFilter();
    }

    onFilterChanged(newFilterHeaders) {
        this.flightService.loadFlights(newFilterHeaders).then((flights) => this.flights = flights);
    }

    private buildColumnFilter():void {
        this.filterDefinition = [];
        this.filterDefinition.push(new ColumnFilters.StringColumnFilter("name"));
        this.filterDefinition.push(new ColumnFilters.DateTimeColumnFilter("dateOfDepartureFrom"));
        this.filterDefinition.push(new ColumnFilters.DateTimeColumnFilter("dateOfDepartureTo"));
        // this.filterDefinition.push(new ColumnFilters.NumberColumnFilter("distance"));
        // this.filterDefinition.push(new ColumnFilters.NumberColumnFilter("seats"));
    }
}

@Component({
    selector: 'flight',
    directives: [RouterLink, NgFor, NgIf, FORM_DIRECTIVES],
    providers: [DetailService, DestinationService],
    templateUrl: 'app/components/flights/flight.html'
})
export class FlightComponent {
    private detailService:DetailService;
    private destinationService:DestinationService;

    title:string;
    destinationOptions:Object[];
    flight:Flight;

    constructor(routeParams:RouteParams, detailService:DetailService, destinationService:DestinationService) {
        this.detailService = detailService;
        this.destinationService = destinationService;

        this.title = 'Flight detail';
        this.flight = new Flight();
        this.destinationOptions = [];
        destinationService.loadDestinationOptions().then((destinationOptions) => {
            this.destinationOptions = destinationOptions
        });

        var entityId = routeParams.get("id");
        if (entityId) {
            detailService.fetchDetail(entityId, FlightService.ENTITY_ENDPOINT, Flight).then((result) => {
                this.flight = <Flight>result
            });
        }
    }

    saveFlight(flight:Flight):void {
        console.log("saving flight ...");
        this.detailService.saveDetail(flight, FlightService.ENTITY_ENDPOINT);
    }

    deleteFlight(flight:Flight):void {
        this.detailService.deleteEntity(flight, FlightService.ENTITY_ENDPOINT);
    }
}