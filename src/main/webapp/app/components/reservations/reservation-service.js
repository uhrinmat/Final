var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require('angular2/angular2');
var http_1 = require('angular2/http');
var common_1 = require("../common/common");
var detail_service_1 = require('../../services/detail-service');
var ReservationService = (function () {
    function ReservationService(http, commonService) {
        this.http = http;
        this.commonService = commonService;
    }
    ReservationService.prototype.loadReservations = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(ReservationService.DESTINATIONS_URL)
                .subscribe(function (result) {
                var reservations = result.json();
                resolve(reservations.map(function (item) {
                    var reservation = new Reservation();
                    _this.commonService.copyOwnObjectProperties(item, reservation);
                    return reservation;
                }));
            });
        });
    };
    ReservationService.DESTINATIONS_URL = common_1.API_URL + '/reservation';
    ReservationService = __decorate([
        angular2_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, common_1.CommonService])
    ], ReservationService);
    return ReservationService;
})();
exports.ReservationService = ReservationService;
var Reservation = (function (_super) {
    __extends(Reservation, _super);
    function Reservation() {
        _super.call(this);
    }
    return Reservation;
})(detail_service_1.BaseEntityDetail);
exports.Reservation = Reservation;
//# sourceMappingURL=reservation-service.js.map