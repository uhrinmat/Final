import { Component, View, NgFor, NgIf, NgClass, NgModel, FORM_DIRECTIVES, FormBuilder, ControlGroup, Validators } from 'angular2/angular2';
import { RouterLink, RouteParams, RouteConfig } from 'angular2/router';
import {Response, Http} from 'angular2/http';
import {EntityDetail, BaseEntityDetail, DetailService} from "../../services/detail-service";
import {API_URL} from '../common/common';
import {Reservation, ReservationService} from "./reservation-service";
import {IdName} from "../common/common";
import {FlightService} from "../flights/flight-service";

@Component({
    selector: 'reservations',
    directives: [RouterLink, NgFor],
    providers: [DetailService, ReservationService],
    templateUrl: 'app/components/reservations/reservations.html'
})
export class ReservationsComponent {
    title:string;
    reservations:Reservation[];

    constructor(reservationService:ReservationService) {
        this.title = 'Reservations';
        this.reservations = [];

        reservationService.loadReservations().then((reservations) =>  this.reservations = <Reservation[]>reservations);
    }
}

@Component({
    selector: 'reservation',
    directives: [FORM_DIRECTIVES, RouterLink, NgFor, NgIf],
    providers: [DetailService, FlightService],
    templateUrl: 'app/components/reservations/reservation.html'
})
export class ReservationComponent {
    // TODO to service
    static ENTITY_ENDPOINT:string = API_URL + "/reservation";

    private detailService:DetailService;

    title:string;
    reservation:Reservation;
    flightOptions:IdName<number>[];

    constructor(routeParams:RouteParams, detailService:DetailService, flightService:FlightService) {
        this.detailService = detailService;

        this.title = 'Reservation detail';
        this.reservation = new Reservation();
        this.flightOptions = [];

        var entityId = routeParams.get("id");
        if (entityId) {
            detailService.fetchDetail(entityId, ReservationComponent.ENTITY_ENDPOINT, Reservation).then((result) => {
                this.reservation = <Reservation>result
            });
        }

        flightService.loadFlightOptions().then((flightOptions) => this.flightOptions = flightOptions);
    }

    saveReservation(reservation:Reservation):void {
        console.log("saving reservation ...");
        this.detailService.saveDetail(reservation, ReservationComponent.ENTITY_ENDPOINT);
    }

    deleteReservation(reservation:Reservation):void {
        this.detailService.deleteEntity(reservation, ReservationComponent.ENTITY_ENDPOINT);
    }
}