var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require('angular2/angular2');
var SortByDirective = (function () {
    function SortByDirective(el) {
        var _this = this;
        this.sortProperty = el.nativeElement.getAttribute('sort-by');
        el.nativeElement.addEventListener('click', function (event) { return _this.elementClicked(event); });
        this.sorted = new angular2_1.EventEmitter();
    }
    SortByDirective.prototype.elementClicked = function (event) {
        event.preventDefault();
        this.sorted.next(this.sortProperty); //Raise clicked event
    };
    SortByDirective = __decorate([
        angular2_1.Directive({
            selector: '[sort-by]',
            outputs: ['sorted']
        }), 
        __metadata('design:paramtypes', [angular2_1.ElementRef])
    ], SortByDirective);
    return SortByDirective;
})();
exports.SortByDirective = SortByDirective;
//# sourceMappingURL=sortby-directive.js.map