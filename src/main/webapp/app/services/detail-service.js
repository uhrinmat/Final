var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require("angular2/angular2");
var http_1 = require("angular2/http");
var common_1 = require("../components/common/common");
var DetailService = (function () {
    function DetailService(http, commonService) {
        this.http = http;
        this.commonService = commonService;
    }
    DetailService.prototype.saveDetail = function (detail, entityEndpoint) {
        var _this = this;
        var requestOptions = new http_1.RequestOptions({
            method: detail.getEntityId() ? http_1.RequestMethods.Put : http_1.RequestMethods.Post,
            headers: new http_1.Headers({ 'Content-Type': 'application/json;charset=UTF-8' }),
            url: entityEndpoint + (detail.getEntityId() ? '/' + detail.getEntityId() : ''),
            body: JSON.stringify(detail)
        });
        console.log("Saving detail. Endpoint url: \"" + entityEndpoint + "\". Method: " + requestOptions.method);
        return new Promise(function (resolve, reject) {
            _this.http.request(new http_1.Request(requestOptions)).subscribe(function () {
                // TODO redirect
                console.log("Successfully saved.");
                resolve();
            }, function () {
                console.log("An error occurred while saving.");
                reject();
            });
        });
    };
    ;
    // TODO reuse from saveDetail
    DetailService.prototype.deleteEntity = function (detail, entityEndpoint) {
        var requestOptions = new http_1.RequestOptions({
            method: http_1.RequestMethods.Delete,
            headers: new http_1.Headers({ 'Content-Type': 'application/json;charset=UTF-8' }),
            url: entityEndpoint + (detail.getEntityId() ? '/' + detail.getEntityId() : '')
        });
        console.log("Deleting entity. Endpoint url: \"" + entityEndpoint + "\". Method: " + requestOptions.method);
        this.http.request(new http_1.Request(requestOptions)).subscribe(function () {
            // TODO redirect
            console.log("Successfully saved flight.");
        }, function () {
            console.log("An error occurred while saving.");
        });
    };
    ;
    DetailService.prototype.fetchDetail = function (entityId, entityEndpoint, type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(entityEndpoint + '/' + entityId).subscribe(function (response) {
                var obj = new type();
                var responseEntity = response.json();
                _this.commonService.copyOwnObjectProperties(responseEntity, obj);
                resolve(obj);
            }, function () {
                alert("Loading entity failed!");
                reject();
            });
        });
    };
    DetailService = __decorate([
        angular2_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, common_1.CommonService])
    ], DetailService);
    return DetailService;
})();
exports.DetailService = DetailService;
var BaseEntityDetail = (function () {
    function BaseEntityDetail() {
    }
    BaseEntityDetail.prototype.getEntityId = function () {
        return this.id;
    };
    return BaseEntityDetail;
})();
exports.BaseEntityDetail = BaseEntityDetail;
//# sourceMappingURL=detail-service.js.map