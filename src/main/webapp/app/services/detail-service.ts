import {Injectable, Inject} from "angular2/angular2";
import {HTTP_PROVIDERS, Http, Request, Response, RequestMethods, RequestOptions, Headers} from "angular2/http";
import {CommonService} from "../components/common/common";

@Injectable()
export class DetailService {
    http:Http;
    commonService:CommonService;

    constructor(http:Http, commonService:CommonService) {
        this.http = http;
        this.commonService = commonService;
    }

    saveDetail(detail:EntityDetail, entityEndpoint:string):Promise<void> {
        var requestOptions:RequestOptions = new RequestOptions({
            method: detail.getEntityId() ? RequestMethods.Put : RequestMethods.Post,
            headers: new Headers({'Content-Type': 'application/json;charset=UTF-8'}),
            url: entityEndpoint + (detail.getEntityId() ? '/'+detail.getEntityId() : ''),
            body: JSON.stringify(detail)
        });
        console.log("Saving detail. Endpoint url: \"" + entityEndpoint + "\". Method: " + requestOptions.method);

        return new Promise<void>((resolve, reject) => {
            this.http.request(new Request(requestOptions)).subscribe(() => {
                // TODO redirect
                console.log("Successfully saved.");
                resolve();
            }, () => {
                console.log("An error occurred while saving.");
                reject();
            });
        });
    };

    // TODO reuse from saveDetail
    deleteEntity(detail:EntityDetail, entityEndpoint:string):void {
        var requestOptions:RequestOptions = new RequestOptions({
            method: RequestMethods.Delete,
            headers: new Headers({'Content-Type': 'application/json;charset=UTF-8'}),
            url: entityEndpoint + (detail.getEntityId() ? '/'+detail.getEntityId() : '')
        });
        console.log("Deleting entity. Endpoint url: \"" + entityEndpoint + "\". Method: " + requestOptions.method);

        this.http.request(new Request(requestOptions)).subscribe(() => {
            // TODO redirect
            console.log("Successfully saved flight.");
        }, () => {
            console.log("An error occurred while saving.");
        });
    };

    fetchDetail(entityId:string|number, entityEndpoint:string,  type): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.get(entityEndpoint + '/' + entityId).subscribe((response:Response) => {
                var obj = new type();
                var responseEntity = response.json();
                this.commonService.copyOwnObjectProperties(responseEntity, obj);
                resolve(obj);
            }, () => {
                alert("Loading entity failed!");
                reject();
            });
        });
    }
}

export interface EntityDetail {
    getEntityId() : number;
}

export abstract class BaseEntityDetail implements EntityDetail {
    id:number;

    getEntityId():number {
        return this.id;
    }
}